REAL FUNCTION f(x)
	REAL x
	f = x**2 - 4
END FUNCTION f

REAL FUNCTION fp(x)
	REAL x
	fp = 2 * x
END FUNCTION fp

PROGRAM newton
	REAL x0, x1, f0, f1, i, e
	x0 = 5
	i = 64
	x1 = x0 - 1
	f0 = f(x0)
	e = 1.0e-4
	DO WHILE ((i > 0).AND.(ABS(x1 - x0) > e).AND.(ABS(f0) > e))
		f1 = fp(x0)
		IF (ABS(f1) < e) THEN
			WRITE(*, *) "Wrong start point"
			STOP
		END IF
		x1 = x0
		x0 = x0 - (f0 / f1)
		f0 = f(x0)
		i = i - 1
		IF (i == 0) THEN
			WRITE(*, *) "Too many iterations"
			STOP
		END IF
	END DO
	WRITE(*, *) x0
END PROGRAM newton
